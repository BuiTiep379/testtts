package org.example;

public class MyBigNumber {
    private String stn1;
    private String stn2;

    public MyBigNumber(String stn1, String stn2) {
        this.stn1 = stn1;
        this.stn2 = stn2;
    }

    public String sum() {
        int maxIndex = 0;
        String total = "";

        maxIndex = Math.max(this.stn1.length(), this.stn2.length());
        int raw = 0;
        for (int i = 0; i < maxIndex; i++) {
            int a = getEndNumber(this.stn1, i+1);
            int b = getEndNumber(this.stn2, i+1);
            int rawSum = a + b + raw;
            if (rawSum >= 10) {
                raw = getStartNumber(rawSum);
                rawSum = rawSum - raw*10;
            } else {
                raw = 0;
            }
            total = String.valueOf(rawSum).concat(total);
        }

        if (raw > 0) {
            total= String.valueOf(raw).concat(total);
        }
        return total;
    }
    public static int getEndNumber(String number, int index) {
        if (index > number.length()) {
            return 0;
        } else if (index == number.length() - 1 && number.length() == 1) {
            return Integer.parseInt(number);
        } else {
            return Character.getNumericValue(number.charAt(number.length() - index));
        }
    }
    public static int getStartNumber(int number) {
        int first_digit = 0;
        while(number > 0) {
            first_digit = number % 10;
            number = number/10;
        }
        return first_digit;
    }
    public String getStn1() {
        return stn1;
    }

    public String getStn2() {
        return stn2;
    }

    public void setStn1(String stn1) {
        this.stn1 = stn1;
    }

    public void setStn2(String stn2) {
        this.stn2 = stn2;
    }
}
