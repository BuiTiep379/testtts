import junit.framework.TestCase;
import org.example.MyBigNumber;
import org.junit.Assert;
import org.junit.Test;

public class MyBigNumberTest {
    public MyBigNumberTest() {
        super();
    }
    @Test
    public void testGetSum1() {
        MyBigNumber myBigNumber = new MyBigNumber("12", "24");
        Assert.assertEquals("36", myBigNumber.sum());
    }
    @Test
    public void testGetSum2() {
        MyBigNumber myBigNumber = new MyBigNumber("1", "2");
        Assert.assertEquals("6", myBigNumber.sum());
    }

    @Test
    public void testGetSum3() {
        MyBigNumber myBigNumber = new MyBigNumber("109", "29");
        Assert.assertEquals("138", myBigNumber.sum());
    }
}
